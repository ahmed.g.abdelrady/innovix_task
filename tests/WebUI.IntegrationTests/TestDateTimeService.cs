﻿using Innovix.Application.Common.Interfaces;
using System;

namespace Innovix.WebUI.IntegrationTests
{
    public class TestDateTimeService : IDateTime
    {
        public DateTime Now => DateTime.Now;
    }
}
