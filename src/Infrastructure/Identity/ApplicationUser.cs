﻿using Microsoft.AspNetCore.Identity;

namespace Innovix.Infrastructure.Identity
{
    public class ApplicationUser : IdentityUser
    {
    }
}
