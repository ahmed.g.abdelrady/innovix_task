﻿using Innovix.Application.Common.Interfaces;
using System;

namespace Innovix.Infrastructure.Services
{
    public class DateTimeService : IDateTime
    {
        public DateTime Now => DateTime.Now;
    }
}
