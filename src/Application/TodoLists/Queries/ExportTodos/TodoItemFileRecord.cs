﻿using Innovix.Application.Common.Mappings;
using Innovix.Domain.Entities;

namespace Innovix.Application.TodoLists.Queries.ExportTodos
{
    public class TodoItemRecord : IMapFrom<TodoItem>
    {
        public string Title { get; set; }

        public bool Done { get; set; }
    }
}
