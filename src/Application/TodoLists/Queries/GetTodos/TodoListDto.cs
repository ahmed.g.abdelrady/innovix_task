﻿using Innovix.Application.Common.Mappings;
using Innovix.Domain.Entities;
using System.Collections.Generic;

namespace Innovix.Application.TodoLists.Queries.GetTodos
{
    public class TodoListDto : IMapFrom<TodoList>
{
    public TodoListDto()
    {
        Items = new List<TodoItemDto>();
    }

    public int Id { get; set; }

    public string Title { get; set; }

    public IList<TodoItemDto> Items { get; set; }
}
}
